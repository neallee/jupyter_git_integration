from IPython.html.utils import url_path_join as ujoin
from .github_commit_push import GitCommitHandler


def load_jupyter_server_extension(nbapp):
    nbapp.log.info('Loaded Jupyter extension: Git Commit and Push')

    webapp = nbapp.web_app
    base_url = webapp.settings['base_url']
    webapp.add_handlers(".*$", [
        (ujoin(base_url, r"/git/commit"), GitCommitHandler,
            {'log': nbapp.log}),
    ])

def _jupyter_server_extension_paths():
    return [{
        "module": "jupyter_git_integration"
    }]
