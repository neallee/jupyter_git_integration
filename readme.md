# Jupyter and Git Integration #
A custom Jupyter extension to commit and push directly to Git from a notebook. 
The extension has two core components:
* A new button on the frontend (aka. nbextension), implemented in Javascript, captures the user’s commit message and name of the current notebook
* A backend Python script (aka. server extension) receives that data and uses the gitpython module to automate the following tasks:
    1. Check out a new branch
    1. Commit changes to the identified notebook
    1. Push commits to GitHub

## Note ##
Tested on Anaconda3-5.1.0-Linux-x86_64, Python 3.6.4 | (default, Jan 16 2018, 18:10:19)

## Install nbextension ##
    cd <parent directory of jupyter_git_integration>
    
    # uninstall if jupyter_git_integration was installed before
    sudo jupyter nbextension uninstall jupyter_git_integration
    
    # install nbextension
    sudo jupyter nbextension install jupyter_git_integration --symlink
    
    # enable nbextension
    jupyter nbextension enable jupyter_git_integration/github-commit-push

## Install server extension ##
    cd <parent directory of jupyter_git_integration>
    
    # uninstall first if jupyter_git_integration was installed before
    pip uninstall jupyter_git_integration -y
    
    # intsall jupyter_git_integration
    pip install <absolute path to jupyter_git_integration>
    
    # enable server extension
    jupyter serverextension enable --py  jupyter_git_integration

## Running Jupyter Notebook with jupyter_git_integration extension
    0. Edit environment variable in <absolute path to jupyter_git_integration>/run_notebook.sh
    1. Go to the directory which you would like to edit and it is tracked by Git
    2. Run <absolute path to jupyter_git_integration>/run_notebook.sh

## Snapshots ##
### Once the installation is correct, a Git icon will show up ###
![](./image/jupyter_git_integration_icon.png)

### Click the Git icon to commit and push changes ###
![](./image/jupyter_git_integration_commit_push.png)

### Done ###
![](./image/jupyter_git_integration_done.png)

