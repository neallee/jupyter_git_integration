from setuptools import setup, find_packages

try:
    long_desc = open('README.md').read()
except:
    long_desc = ''

setup(
    name='jupyter_git_integration',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
)
